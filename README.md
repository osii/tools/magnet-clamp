# Magnet Clamp

## Overview

This clamp may be a useful tool when mounting the rings of the [30cm Halbach Magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet) as the rings repel each other.

For a quick 3D view see [here](res/3d/clamp.stl) (NOTE that this shows potentially an _old_ version of the clamp, but you get the point; for STL/STP exports please see the respective [release notes](https://gitlab.com/osii/tools/magnet-wrench/-/releases)).

## Make

You can find the STL export in the [release notes](https://gitlab.com/osii/tools/magnet-clamp/-/releases).

## Use

The clamp is designed for a distance of 8 rings (14mm each + 6mm spacer sleeves in between).

So instead of 6…8 hands you can just use these clamps:

![](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/raw/9823fc2d3a3258222bd331544d99f385b855cf13/res/img/clamped-rings.jpg){width=60%}

## Contributors (alphabetical order)

Martin Häuer

## License and Liability

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and [DISCLAIMER](DISCLAIMER.pdf).

